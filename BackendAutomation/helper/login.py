import json
import requests


class LoginApi:

    @staticmethod
    def get_random_id(email, krista_grand_url, application="client"):
        url = krista_grand_url + "/rpc/v1/auth/verifyEmail"
        payload = {
            "payload": {
                "identificationToken": {"tokenType": "email", "value": email},
                "sendEmail": False,
                "appVersion": {
                    "name": "Krista",
                    "version": "0.1",
                    "build": "qa"
                },
                "application": application
            }
        }

        payload = json.dumps(payload)
        headers = {
            'Content-Type': 'application/json'
        }

        response = requests.request("POST", url, headers=headers, data=payload, verify=False)

        if response.status_code != 200:
            raise Exception(json.dumps(response))

        response = json.loads(response.text)

        if response.get("isError"):
            raise Exception(str(response.get("payload").get("validationErrors")))

        login_code = response["payload"]["loginCode"]
        return login_code

    @staticmethod
    def get_login_url_for_studio(email, krista_grand_url, krista_studio_url):
        login_code = LoginApi.get_random_id(email, krista_grand_url, application="studio")
        studio_link = "{}/confirm?code={}&type=loginLink".format(krista_studio_url, login_code)
        return studio_link

    @staticmethod
    def get_login_url_for_client(email, krista_grand_url, krista_client_url):
        login_code = LoginApi.get_random_id(email, krista_grand_url, application="client")
        client_link = "{}/confirm?code={}&type=loginLink".format(krista_client_url, login_code)
        return client_link

    @staticmethod
    def get_new_client_session_id(email, workspace_id, password, krista_grand_url):
        login_code = LoginApi.get_random_id(email, krista_grand_url, application="client")
        url = krista_grand_url + "/rpc/v1/auth/login"
        payload = {
             "payload": {
                "identificationToken": {"tokenType" : "email", "value" : email},
                "loginCode": login_code,
                "workspaceId":workspace_id,
                "password": password,
                "appVersion":{
                    "name":"Krista",
                    "version":"0.1",
                    "build":"qa"
                },
                "application":"client"
            }
        }

        payload = json.dumps(payload)
        headers = {
            'Content-Type': 'application/json'
        }

        response = requests.request("POST", url, headers=headers, data=payload, verify=False)

        if response.status_code != 200:
            raise Exception(json.dumps(response))

        response = json.loads(response.text)

        if response.get("isError"):
            raise Exception(str(response.get("payload").get("validationErrors")))

        client_session_id = response["payload"]["clientSessionId"]
        return client_session_id






if __name__ == "__main__":
    # print(LoginApi.get_random_id("tushar.singhal@antbrains.com",
    #                              "http://krista.qa.antbrains.com:31897")
    #       )
    #
    # print(
    #     LoginApi.get_login_url_for_client("tushar.singhal@antbrains.com",
    #                                       krista_grand_url="http://krista.qa.antbrains.com:31897",
    #                                       krista_client_url="http://krista.qa.antbrains.com:31967"
    #                                       )
    # )
    #
    # print(
    #     LoginApi.get_login_url_for_studio("tushar.singhal@antbrains.com",
    #                                       krista_grand_url="http://krista.qa.antbrains.com:31897",
    #                                       krista_studio_url="http://krista.qa.antbrains.com:30629")
    # )

    print(LoginApi.get_new_client_session_id("testkristaautomation@gmail.com", "ws_79a7993d-fb15-4d43-aec1-44337dcd7d57", "Test@1234",
                                             "http://krista.qa.antbrains.com:31897"))