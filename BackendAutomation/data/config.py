class ExecutionEnvironment:
    SETUP = "local"  # local or nightly
    USE_SELENIUM_GRID = False
    SELENIUM_GRID_URL = "http://192.168.10.127:32778/wd/hub"
    # SELENIUM_GRID_URL = "http://127.0.0.1:4444/wd/hub"


class StudioConfig:
    BROWSER = "chrome"  # safari, chrome, firefox
    BASE_URL = "https://studio.tx-qa-01.in.antbrains.com"
    HOME_URL = "{}/home".format(BASE_URL)

    if ExecutionEnvironment.SETUP == "nightly":
        BROWSER = "STUDIO_BROWSER"  # safari, chrome, firefox
        BASE_URL = "STUDIO_BASE_URL"
        HOME_URL = "{}/home".format(BASE_URL)


class ClientConfig:
    BROWSER = "chrome"
    BASE_URL = "https://client.tx-qa-01.in.antbrains.com"
    HOME_URL = "{}/login".format(BASE_URL)
    WEBSOCKET_URL = "wss://client.tx-qa-01.in.antbrains.com"

    if ExecutionEnvironment.SETUP == "nightly":
        BROWSER = "WEBCLIENT_BROWSER"
        BASE_URL = "WEBCLIENT_BASE_URL"
        HOME_URL = "{}/login".format(BASE_URL)
        WEBSOCKET_URL = "WEBCLIENT_WEBSOCKET_URL"


class GrandConfig:
    BASE_URL = "https://accounts.tx-qa-01.in.antbrains.com"

    if ExecutionEnvironment.SETUP == "nightly":
        BASE_URL = "GRAND_BASE_URL"




