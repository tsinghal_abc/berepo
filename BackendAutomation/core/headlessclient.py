from helper.login import LoginApi
from data.config import ExecutionEnvironment, GrandConfig, ClientConfig
from data.workspacedata import WORKSPACE
import websocket, time, json
import uuid
import requests
import urllib.parse


class HeadlessClient:

    def __init__(self, client_session_id=None):
        self.ws = None
        self.client_session_id = None
        if client_session_id:
            self.client_session_id = client_session_id
        self.channels = None  # List of disctionary with keys "channelId", "name", "unreadMessages", "description", "lastMessage"
        self.is_ws_connection_active = False
        self.drain_inbox = True
        self.last_messages_all = {}  # key is inbox_id
        self.last_attention_needed_messages = {}  # key is inbox_id
        self.last_text_messages = {}
        self.page_size = 20
        self.drain_started = False

    def wait_for_draining_to_finish(self):
        time.sleep(2)
        while self.drain_started:
            print("Waiting for draining to finish")
            time.sleep(10)

    def get_channel_id_from_name(self, channel_name):
        if not self.channels:
            return None

        for ch in self.channels:
            if ch["name"] == channel_name:
                return ch["channelId"]

    def is_channel_present_in_workspace(self, channel_id):
        if not self.channels:
            return False

        is_channel_found = False
        for ch in self.channels:
            if ch["channelId"] == channel_id:
                is_channel_found = True
                break
        return is_channel_found

    def set_new_client_session_id(self, email, password, ws_id=None):
        # email = "testkristaautomation@gmail.com"
        # ws_id = "ws_79a7993d-fb15-4d43-aec1-44337dcd7d57"
        # password = "Test@1234"
        ws_id = WORKSPACE.WS_ID
        self.client_session_id = LoginApi.get_new_client_session_id(email,
                                                                    ws_id,
                                                                    password,
                                                                    GrandConfig.BASE_URL)

        print(self.client_session_id)

    def get_channels(self):
        workspace_url = "%s/api/rpc/v1/client/workspace/%s" % (ClientConfig.BASE_URL, WORKSPACE.WS_ID)
        payload = {}
        session_string = {"clientSessionId": self.client_session_id,
                          "transactionId": "req_{}".format(str(uuid.uuid4())), "transactionStartTime": int(time.time())}
        headers = {
            'Host': '%s' % (ClientConfig.BASE_URL.split("://")[-1]),
            'Referer': '%s' % workspace_url,
            'X-Krista-Context': urllib.parse.quote(str(session_string))
        }
        response = requests.request("GET", workspace_url, headers=headers, data=payload, verify=False)
        response_json = json.loads(response.text)
        if response.status_code != 200 or response_json.get("isError"):
            raise Exception(
                "{} : {}".format(response.status_code, json.dumps(response_json["payload"].get("validationErrors"))))

        self.channels = response_json["payload"]["channels"]
        return self.channels

        '''
        curl 'http://krista.qa.antbrains.com:31967/api/rpc/v1/client/workspace/ws_fa3c1a2c-9a37-4952-b0d7-be8333d12075' \
            -X 'GET' \
            -H 'Content-Type: application/json; charset=UTF-8' \
            -H 'Pragma: no-cache' \
            -H 'Accept: application/json, text/plain, */*' \
            -H 'Host: krista.qa.antbrains.com:31967' \
            -H 'Accept-Language: en-us' \
            -H 'Accept-Encoding: gzip, deflate' \
            -H 'Cache-Control: no-cache' \
            -H 'Referer: http://krista.qa.antbrains.com:31967/workspace/ws_fa3c1a2c-9a37-4952-b0d7-be8333d12075/defnMult_d4e810fd-5287-4db8-9cff-263d06aa4461?view=timeline' \
            -H 'Connection: keep-alive' \
            -H 'User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.1.2 Safari/605.1.15' \
            -H 'Cookie: io=k0uiuS-7eoble-X0AAJs' \
            -H 'X-Krista-Context: %7B%22clientSessionId%22%3A%22device_c1602e0a-916e-458f-b0ab-c0b3bd716fa8%22%2C%22transactionId%22%3A%22req_598b3cf6-dbde-4b9d-8a58-d06f21611d9f%22%2C%22transactionStartTime%22%3A1597684868751%7D'


        :return:
        '''

    def get_channel_summary(self, channel_id):
        if self.channels is None:
            self.channels = self.get_channels()

        workspace_url = "%s/api/rpc/v1/client/workspace/%s" % (ClientConfig.BASE_URL, WORKSPACE.WS_ID)

        workspace_channel_url = "%s/channel/%s/summary" % (workspace_url, channel_id)
        payload = {}
        session_string = {"clientSessionId": self.client_session_id,
                          "transactionId": "req_{}".format(str(uuid.uuid4())), "transactionStartTime": int(time.time())}
        headers = {
            'Host': '%s' % (ClientConfig.BASE_URL.split("://")[-1]),
            'Referer': '%s/%s' % (workspace_url, channel_id),
            'X-Krista-Context': urllib.parse.quote(str(session_string))
        }
        response = requests.request("GET", workspace_channel_url, headers=headers, data=payload, verify=False)
        if response.status_code != 200:
            raise Exception("{} : {}".format(response.status_code, response.text))
        response_json = json.loads(response.text)
        if response.status_code != 200 or response_json.get("isError"):
            raise Exception(
                "{} : {}".format(response.status_code, json.dumps(response_json["payload"].get("validationErrors"))))

        # sample response
        # {"context": {"transactionId": "req_c1d7a730-3eca-4515-93cc-7da83eba2cbc", "transactionStartTime": 1598187304,
        #              "serverTimeTaken": 33},
        #  "payload": {"unreadMessages": 0, "activeConversations": 0, "attentionNeeded": 0}, "isError": false}

        return response_json["payload"]

        '''
        curl 'http://krista.qa.antbrains.com:31967/api/rpc/v1/client/workspace/ws_fa3c1a2c-9a37-4952-b0d7-be8333d12075/channel/defnMult_d4e810fd-5287-4db8-9cff-263d06aa4461/summary' \
            -X 'GET' \
            -H 'Content-Type: application/json; charset=UTF-8' \
            -H 'Pragma: no-cache' \
            -H 'Accept: application/json, text/plain, */*' \
            -H 'Host: krista.qa.antbrains.com:31967' \
            -H 'Accept-Language: en-us' \
            -H 'Accept-Encoding: gzip, deflate' \
            -H 'Cache-Control: no-cache' \
            -H 'Referer: http://krista.qa.antbrains.com:31967/workspace/ws_fa3c1a2c-9a37-4952-b0d7-be8333d12075/defnMult_d4e810fd-5287-4db8-9cff-263d06aa4461' \
            -H 'Connection: keep-alive' \
            -H 'User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.1.2 Safari/605.1.15' \
            -H 'Cookie: io=ym4Ps3GQTawi8zdFAAJz' \
            -H 'X-Krista-Context: %7B%22clientSessionId%22%3A%22device_c1602e0a-916e-458f-b0ab-c0b3bd716fa8%22%2C%22transactionId%22%3A%22req_aa1a4b55-c516-4b35-859c-6fb83e97e455%22%2C%22transactionStartTime%22%3A1597685342864%7D'

            :param channel_name:
            :return:
            '''

    def get_conversations_in_channel(self, channel_id):
        if self.channels is None:
            self.channels = self.get_channels()

        workspace_url = "%s/api/rpc/v1/client/workspaces/%s" % (ClientConfig.BASE_URL, WORKSPACE.WS_ID)

        workspace_channel_url = "%s/channels/%s/conversationsDefinitions?offset=0&&limit=0" % (
            workspace_url, channel_id)
        payload = {}
        session_string = {"clientSessionId": self.client_session_id,
                          "transactionId": "req_{}".format(str(uuid.uuid4())), "transactionStartTime": int(time.time())}
        headers = {
            'Host': '%s' % (ClientConfig.BASE_URL.split("://")[-1]),
            'Referer': '%s/%s?view=timeline' % (workspace_url, channel_id),
            'X-Krista-Context': urllib.parse.quote(str(session_string))
        }
        response = requests.request("GET", workspace_channel_url, headers=headers, data=payload, verify=False)

        if response.status_code != 200:
            raise Exception("{} : {}".format(response.status_code, response.text))
        response_json = json.loads(response.text)
        if response_json.get("isError"):
            raise Exception(
                "{} : {}".format(response.status_code, json.dumps(response_json["payload"].get("validationErrors"))))

        # sample response
        # {"context": {"transactionId": "req_c1d7a730-3eca-4515-93cc-7da83eba2cbc", "transactionStartTime": 1598187304,
        #              "serverTimeTaken": 33},
        #  "payload": {"unreadMessages": 0, "activeConversations": 0, "attentionNeeded": 0}, "isError": false}

        # self.conversations_list = response_json["payload"]["conversations"]
        return response_json["payload"][
            "conversations"]  # [{'conversationDefinitionId': 'CD_b66d7bed-ed46-481d-9852-b2a3fda63e2f', 'title': 'Conversation - 1', 'description': 'cc'}]

        '''curl 'http://krista.qa.antbrains.com:31967/api/rpc/v1/client/workspaces/ws_fa3c1a2c-9a37-4952-b0d7-be8333d12075/channels/defnMult_d4e810fd-5287-4db8-9cff-263d06aa4461/conversationsDefinitions?offset=0&&limit=0' \
            -X 'GET' \
            -H 'Content-Type: application/json; charset=UTF-8' \
            -H 'Pragma: no-cache' \
            -H 'Accept: application/json, text/plain, */*' \
            -H 'Host: krista.qa.antbrains.com:31967' \
            -H 'Accept-Language: en-us' \
            -H 'Accept-Encoding: gzip, deflate' \
            -H 'Cache-Control: no-cache' \
            -H 'Referer: http://krista.qa.antbrains.com:31967/workspace/ws_fa3c1a2c-9a37-4952-b0d7-be8333d12075/defnMult_d4e810fd-5287-4db8-9cff-263d06aa4461?view=timeline' \
            -H 'Connection: keep-alive' \
            -H 'User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.1.2 Safari/605.1.15' \
            -H 'Cookie: io=EZg0Zj1fuIhuI8GVAAJr' \
            -H 'X-Krista-Context: %7B%22clientSessionId%22%3A%22device_c1602e0a-916e-458f-b0ab-c0b3bd716fa8%22%2C%22transactionId%22%3A%22req_a134675c-3e62-4a8d-bfad-4a25236464e4%22%2C%22transactionStartTime%22%3A1597684763492%7D'

            '''

    def get_conversation_initial_form(self, channel_id, conversation_id):
        workspace_url = "%s/api/rpc/v1/client/workspace/%s" % (ClientConfig.BASE_URL, WORKSPACE.WS_ID)

        workspace_conversation_def_url = "%s/channel/%s/conversationDefinition/%s" % (
            workspace_url, channel_id, conversation_id)
        payload = {}
        session_string = {"clientSessionId": self.client_session_id,
                          "transactionId": "req_{}".format(str(uuid.uuid4())), "transactionStartTime": int(time.time())}
        headers = {
            'Host': '%s' % (ClientConfig.BASE_URL.split("://")[-1]),
            'Referer': '%s/%s?view=timeline' % (workspace_url, channel_id),
            'X-Krista-Context': urllib.parse.quote(str(session_string))
        }
        response = requests.request("GET", workspace_conversation_def_url, headers=headers, data=payload, verify=False)

        if response.status_code != 200:
            raise Exception("{} : {}".format(response.status_code, response.text))
        response_json = json.loads(response.text)
        if response_json.get("isError"):
            raise Exception(
                "{} : {}".format(response.status_code, json.dumps(response_json["payload"].get("validationErrors"))))

        conversation_form_payload = response_json["payload"]
        return conversation_form_payload

    def submit_conversation_step_form(self, channel_id, conversation_id, processed_payload_json):
        workspace_url = "%s/api/rpc/v1/client/workspace/%s" % (ClientConfig.BASE_URL, WORKSPACE.WS_ID)
        workspace_conversation_def_url = "%s/channel/%s/conversationDefinition/%s" % (
            workspace_url, channel_id, conversation_id)
        session_string = {"clientSessionId": self.client_session_id,
                          "transactionId": "req_{}".format(str(uuid.uuid4())), "transactionStartTime": int(time.time())}
        headers = {
            'Origin': ClientConfig.BASE_URL,
            'Host': '%s' % (ClientConfig.BASE_URL.split("://")[-1]),
            'Referer': '%s/%s?view=timeline' % (workspace_url, channel_id)
        }

        payload = {
            "context": {
                "clientSessionId": self.client_session_id,
                "transactionId": "req_{}".format(str(uuid.uuid4())),
                "transactionStartTime": int(time.time())
            },
            "payload": processed_payload_json

        }
        response = requests.request("POST", workspace_conversation_def_url, headers=headers, data=json.dumps(payload), verify=False)
        print(response.status_code)
        print(response.text)
        '''
        curl 'http://krista.qa.antbrains.com:31967/api/rpc/v1/client/workspace/ws_fa3c1a2c-9a37-4952-b0d7-be8333d12075/channel/defnMult_d4e810fd-5287-4db8-9cff-263d06aa4461/conversationDefinition/CD_c094fe9f-528f-4b77-8ff3-ca1cee174c42' \
        -X 'POST' \
        -H 'Cookie: io=lfcpNtKBZ2NQP9FPAAGY' \
        -H 'Accept: application/json, text/plain, */*' \
        -H 'Content-Type: application/json; charset=UTF-8' \
        -H 'Origin: http://krista.qa.antbrains.com:31967' \
        -H 'Content-Length: 3382' \
        -H 'Accept-Language: en-us' \
        -H 'Host: krista.qa.antbrains.com:31967' \
        -H 'User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.1.2 Safari/605.1.15' \
        -H 'Referer: http://krista.qa.antbrains.com:31967/workspace/ws_fa3c1a2c-9a37-4952-b0d7-be8333d12075/defnMult_d4e810fd-5287-4db8-9cff-263d06aa4461?view=timeline' \
        -H 'Accept-Encoding: gzip, deflate' \
        -H 'Connection: keep-alive' \
        --data-binary '{"context":{"clientSessionId":"device_77d6dea3-6700-45a1-b590-247335161f0c",
        "transactionId":"req_fac7949e-47df-4844-b454-ab1e6cdf5b4e","transactionStartTime":1598192613717},
        "payload":{"messageId":"M_c77c160b-f8e7-43c2-8ccf-f587de2c33d3",
        "messageType":"MT_2010",
        "toInboxId":"inbox_defnMult_d4e810fd-5287-4db8-9cff-263d06aa4461",
        "fromInboxId":"inbox_wsContact_255728d6-daf1-43f6-8bd0-1d27ac8a2313",
        "fromAccountId":"wsContact_255728d6-daf1-43f6-8bd0-1d27ac8a2313",
        "creationTime":1598192586096,
        "attributes":{"message":{"type":"com.krista.net.model.message.TextMessage","message":"Specify PTO details"},
        "paths":[],"form":{"localId":"FORM_56d620db-56ad-4147-b6d3-4234c8de6b5f","pages":[{"type":"com.krista.net.model.form.FormPage","title":"Input","pageFields":[{"localId":"FIELD_75074195-348a-4dc5-a58b-0a3b56000df4","label":"Reason","field":{"type":"Field","fieldType":"com.krista.fields.Text","attributes":{},"nlpHints":[]},"readOnly":false,"hidden":false,"additionalNlpHints":[]},{"localId":"FIELD_e54eb842-7646-41b1-9b4a-5d9195aeb96f","label":"Duration","field":{"type":"Field","fieldType":"com.krista.fields.DateRange","attributes":{"includeTimeOfDay":true,"showHowManyDaysInViewer":60,"allowPast":true,"allowToday":true,"referencingName":["from","to"],"allowFuture":true,"defaultTimeSpan":1,"required":false},"nlpHints":[]},"readOnly":false,"hidden":false,"additionalNlpHints":[]}]}],"visibilityRules":[{"type":"com.krista.net.model.form.VisibilityRule","condition":[{"type":"Expression","expression":"","yieldValue":"Boolean","language":"JavaScript","consumedVariables":[]}],"thenActions":[{"type":"com.krista.net.model.form.FieldVisibilityAction","action":"show_field"}],"elseActions":[{"type":"com.krista.net.model.form.FieldVisibilityAction","action":"show_field"}]}],"validationRules":[{"type":"com.krista.net.model.form.FormValidationRule","condition":[{"type":"Expression","expression":"","yieldValue":"Boolean","language":"JavaScript","consumedVariables":[]}],"thenActions":[{"type":"ValidationNotification","severity":"error"}]}],"roleMatrix":{"FIELD_75074195-348a-4dc5-a58b-0a3b56000df4":{"defnRole_65430bcb-f1c9-4b7e-8338-706e47188c04":"write","defnRole_db8c62f0-533b-456a-bb52-feb669dece70":"write","defnRole_720e7043-d91e-4b52-878b-29996a91cb68":"write"},"FIELD_e54eb842-7646-41b1-9b4a-5d9195aeb96f":{"defnRole_65430bcb-f1c9-4b7e-8338-706e47188c04":"write","defnRole_db8c62f0-533b-456a-bb52-feb669dece70":"write","defnRole_720e7043-d91e-4b52-878b-29996a91cb68":"write"}},"readOnly":false,"nlpHints":[]},"formResponse":{"submitType":"submit","submitValue":"","fieldValues":{"FIELD_75074195-348a-4dc5-a58b-0a3b56000df4":"Going to Goa","FIELD_e54eb842-7646-41b1-9b4a-5d9195aeb96f":{"type":"com.krista.net.model.fieldschemas.DateRange","from":1597847003000,"to":1598883803000}}},"sidebars":[]},"stepExecutionMeta":{"type":"com.krista.net.model.conv.StepExecutionMeta","conversationTitle":"Apply PTO","conversationDefinitionId":"CD_c094fe9f-528f-4b77-8ff3-ca1cee174c42","conversationId":"CEC_a08edc51-8300-4641-893a-40e6cc60c364","conversationStepId":"CS_1d9e6593-760c-4f32-8322-ca3a40e8aa8f","branch":["main"],"thread":["main"]},"fromAccount":null,"targetAudience":{"type":"com.krista.net.model.message.TargetAudience","accountIds":["wsContact_255728d6-daf1-43f6-8bd0-1d27ac8a2313","wsContact_258325e1-4ebb-45e8-8466-d7190bbd5108"],"roleIds":[]},"attentionRequired":false}}'


        :return:

        
        '''

    def send_text_message_to_channel(self, channel_id, message):
        send_message = {
            "apiKey": "/v1/chat/SendMessageRequest",
            "payload": {"messageId": "M_{}".format(str(uuid.uuid4())),
                        "messageType": "MT_1010",
                        "workspaceId": WORKSPACE.WS_ID,
                        "inboxId": "inbox_{}".format(channel_id),
                        "attributes": {"message": message}, "replyToMessageMeta": None, "stepExecutionMeta": None,
                        "targetAudience": None}
        }
        self.ws.send(json.dumps(send_message))


        '''
        {"apiKey":"/v1/chat/SendMessageRequest","payload":{"messageId":"M_e022dc04-ad7a-45d8-ab4f-b1995743c632","messageType":"MT_1010","workspaceId":"ws_fa3c1a2c-9a37-4952-b0d7-be8333d12075",
        "inboxId":"inbox_wsContact_dd4c59d8-a60c-4de7-98e6-32124669c5e5","attributes":{"message":"hello"},"replyToMessageMeta":null,"stepExecutionMeta":null,"targetAudience":null}}	1598622209.4338355
        '''

        '''
        {"apiKey":"/v1/chat/SendMessageRequest",
        "payload":{"messageId":"M_e7af3b5e-bb1c-4eb6-8b5c-00cb2d4d42a8",
        "messageType":"MT_1010","workspaceId":"ws_fa3c1a2c-9a37-4952-b0d7-be8333d12075",
        "inboxId":"inbox_defnMult_d4e810fd-5287-4db8-9cff-263d06aa4461",
        "attributes":{"message":"hello"},"replyToMessageMeta":null,"stepExecutionMeta":null,"targetAudience":null}}	1598190201.9355216 
        '''

    def send_text_message_to_person(self, account_id, message):
        self.send_text_message_to_channel(account_id, message)

    def start_web_socket_connection(self):
        self.ws = websocket.WebSocketApp(
            "{}/wsoc/hello".format(ClientConfig.WEBSOCKET_URL),
            on_message=self.on_message,
            on_error=self.on_error,
            on_close=self.on_close,
        )

        self.ws.on_open = self.on_open
        print("Started WebSocket...")
        self.ws.run_forever()

    def close_socket(self):
        self.ws.close()

    def on_open(self):
        auth_request_payload = {"apiKey": "/v1/chat/ChatAuthenticationRequest", "payload": {
            "context": {"clientSessionId": self.client_session_id,
                        "transactionId": "req_{}".format(str(uuid.uuid4())),
                        "transactionStartTime": int(time.time() * 1000)},
            "apiVer": "v1", "apiPath": "chat"}}
        print("Inside _on_open")
        self.ws.send(json.dumps(auth_request_payload))

    def on_close(self):
        self.is_ws_connection_active = False
        print("### closed ###")

    def on_error(self, error):
        self.is_ws_connection_active = False
        print(error)

    def on_message(self, message):

        message = json.loads(message)
        if message["apiKey"] == "/v1/chat/PingResponse":
            send_message = {
                "apiKey": "/v1/chat/PingRequest",
                "payload": {"text": message["payload"]}
            }
            self.ws.send(json.dumps(send_message))

        elif message["apiKey"] == "/v1/chat/ChatAuthenticationResponse":
            if message["payload"] == {}:
                self.is_ws_connection_active = True

        elif message["apiKey"] == "/v1/chat/NewMessagesNotification":
            if self.drain_inbox:
                self.drain_started = True
                workspace = message["payload"]["workspaceId"]
                inboxId = message["payload"]["inboxId"]
                topOffset = message["payload"]["topOffset"]
                send_message = {"apiKey": "/v1/chat/DrainInboxRequest", "payload": {"workspaceId": workspace,
                                                                                    "inboxId": inboxId,
                                                                                    "offset": topOffset - 1,
                                                                                    "pageSize": 20}}  # bind from New message notification

                if inboxId not in self.last_messages_all:
                    self.last_messages_all[inboxId] = []
                if inboxId not in self.last_text_messages:
                    self.last_text_messages[inboxId] = []
                if inboxId not in self.last_attention_needed_messages:
                    self.last_attention_needed_messages[inboxId] = []

                self.ws.send(json.dumps(send_message))

        elif message["apiKey"] == "/v1/chat/DrainInboxResponse":
            for m in message["payload"]["messages"]:
                inbox_id = m["toInboxId"]
                is_attention_required = m["attentionRequired"]
                self.last_messages_all[inbox_id].append(m)

                if m["messageType"] == "MT_1010":
                    # print(self.client_session_id + "--->" + m.get("attributes").get("message"))
                    self.last_text_messages[inbox_id].append(m.get("attributes").get("message"))
                elif m["messageType"] in ["MT_2010"]:
                    if is_attention_required:
                        if message["payload"]["messageCount"] != 2:
                            self.last_attention_needed_messages[inbox_id].append(m)
            self.drain_started = False

        elif message["apiKey"] == "/v1/chat/BadgeNotification":
            pass
            # print(self.client_session_id + "--->" + json.dumps(message))
        elif message["apiKey"] == "/v1/chat/WorkspaceChangedNotification":
            self.get_channels()
        elif message["apiKey"] == "/v1/chat/SendMessageResponse":
            pass
        elif message["apiKey"] == "/v1/chat/MessageUpdateNotification":
            pass
        else:
            pass
            # print(self.client_session_id + "--->" + json.dumps(message))


if __name__ == "__main__":
    import threading

    c1 = HeadlessClient()
    user1 = WORKSPACE.STATIC_USERS["tushar.singhal@antbrains.com"]
    c1.set_new_client_session_id("tushar.singhal@antbrains.com", user1.get("password"))
    t1 = threading.Thread(target=c1.start_web_socket_connection, daemon=True)

    t1.start()
    c1.get_channels()

    channel_id = c1.get_channel_id_from_name("Attendance channel")

    conversations_in_attendance_channel = c1.get_conversations_in_channel(channel_id)
    conv_id = None
    # [{'conversationDefinitionId': 'CD_b66d7bed-ed46-481d-9852-b2a3fda63e2f', 'title': 'Conversation - 1', 'description': 'cc'}]}

    for conv in conversations_in_attendance_channel:
        if conv["title"] == "Apply PTO":
            conv_id = conv['conversationDefinitionId']
            break

    initial_pto_form_json = c1.get_conversation_initial_form(channel_id, conv_id)

    submit_payload = initial_pto_form_json
    submit_payload["fromAccount"] = None
    del submit_payload["type"]
    submit_payload["attributes"]["formResponse"] = {
        "fieldValues": {
            "FIELD_75074195-348a-4dc5-a58b-0a3b56000df4": "TTTTT",
            "FIELD_e54eb842-7646-41b1-9b4a-5d9195aeb96f": {
                "from": 1597246297000,
                "to": 1597246297000,
                "type": "com.krista.net.model.fieldschemas.DateRange"
            }
        },
        "submitType": "submit",
        "submitValue": ""
    }

    c1.submit_conversation_step_form(channel_id, conv_id, submit_payload)

    time.sleep(5)
    last_step_form_json = c1.last_attention_needed_messages["inbox_" + channel_id][-1]

    print(last_step_form_json["messageId"])
    print(last_step_form_json["stepExecutionMeta"])
    target_audience = last_step_form_json["targetAudience"]["accountIds"]

    payload = {
        "messageId": "M_{}".format(str(uuid.uuid4())),
        "messageType": "MT_2020",
        "workspaceId": WORKSPACE.WS_ID,
        "inboxId": "inbox_" + channel_id,
        "attributes": {
            "formResponse": {
                "submitType": "submit",
                "submitValue": None,
                "fieldValues": {
                    "FIELD_7b115ff1-c42e-48b2-aa72-ec1ae354a2f6": "Approved",
                    "FIELD_1dcdee37-79cb-40d9-b12a-e570fbe8a09c": "Test comment222"
                }
            },
            "targetMessageId": last_step_form_json["messageId"]
        },
        "replyToMessageMeta": None,
        "stepExecutionMeta": last_step_form_json["stepExecutionMeta"],
        "targetAudience": { "accountIds" :target_audience }

    }

    send_message = {
        "apiKey": "/v1/chat/SendMessageRequest",
        "payload" : payload
    }

    c1.ws.send(json.dumps(send_message))

    # for ch in c1.channels:
    #     print("{} : {}".format(ch["channelId"], ch["name"]))
    #     print(c1.get_channel_summary(ch["channelId"]))
    #     print(c1.get_conversations_in_channel(ch["channelId"]))
    #
    #     if c1.is_ws_connection_active:
    #         for i in range(5,15):
    #             c1.send_text_message_to_channel(ch["channelId"], ch["channelId"] + "=====> {}".format(i))

    # t1.start()

    time.sleep(5)
    # c2 = HeadlessClient()
    # user2 = WORKSPACE.STATIC_USERS["testkristaautomation@gmail.com"]
    # c2.set_new_client_session_id("testkristaautomation@gmail.com", user2.get("password"))
    # t2 = threading.Thread(target=c2.start_web_socket_connection, daemon=True)
    #
    # c3 = HeadlessClient()
    # user3 = WORKSPACE.STATIC_USERS["testkristaautomation1@gmail.com"]
    # c3.set_new_client_session_id("testkristaautomation1@gmail.com", user3.get("password"))
    # t3 = threading.Thread(target=c3.start_web_socket_connection, daemon=True)
    #
    # # u2 = HeadlessClient()
    # # u2.set_new_client_session_id()
    # # t2 = threading.Thread(target = u2.start_web_socket_connection, daemon=True)
    #
    # t1.start()
    # t2.start()
    # t3.start()
    #
    # # u1.ws.send("Hi I am Python client")
    # # u1.ws.close()
    # # u2.ws.close()
    #
    # t1.join()
    # t2.join()
    # t3.join()
